package command;

import gameClient.MultiplayerActor;
import gameClient.MultiplayerWorld;

public class SetScaleCommand extends ClientCommandHandler {

	@Override
	public void doCommand(String cmd, String[] params, MultiplayerWorld world) {
		if (params.length < 3) throw new IllegalArgumentException("Not enough parameters. Correct params are <id, y>");
		String id = params[0];
		double scaleX = Double.parseDouble(params[1]);
		double scaleY = Double.parseDouble(params[2]);
		MultiplayerActor actor = world.actorWithId(id);
		if (actor != null) {
			actor.setScaleX(scaleX);
			actor.setScaleY(scaleY);
		}
	}

	@Override
	public String getCommandWord() {
		return "S";
	}

	public String getSetScaleCmdStr(String id, double scaleX, double scaleY) {
		return getCommandString(this, new String[]{id, "" + scaleX, "" + scaleY});
	}
}
