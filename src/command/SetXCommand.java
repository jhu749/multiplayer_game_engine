package command;

import gameClient.MultiplayerActor;
import gameClient.MultiplayerWorld;

public class SetXCommand extends ClientCommandHandler {

	@Override
	public void doCommand(String cmd, String[] params, MultiplayerWorld world) {
		if (params.length < 2) throw new IllegalArgumentException("Not enough parameters. Correct params are <id, x>");
		String id = params[0];
		double x = Double.parseDouble(params[1]);
		MultiplayerActor actor = world.actorWithId(id);
		if (actor != null) {
			actor.setX(x);
		}
	}

	@Override
	public String getCommandWord() {
		return "X";
	}

	public String getSetXCmdStr(String id, double x) {
		return getCommandString(this, new String[]{id, "" + x});
	}
}
