package command;

import gameClient.MultiplayerActor;
import gameClient.MultiplayerWorld;

public class SetYCommand extends ClientCommandHandler {

	@Override
	public void doCommand(String cmd, String[] params, MultiplayerWorld world) {
		if (params.length < 2) throw new IllegalArgumentException("Not enough parameters. Correct params are <id, y>");
		String id = params[0];
		double y = Double.parseDouble(params[1]);
		MultiplayerActor actor = world.actorWithId(id);
		if (actor != null) {
			actor.setY(y);
		}
	}

	@Override
	public String getCommandWord() {
		return "Y";
	}

	public String getSetYCmdStr(String id, double y) {
		return getCommandString(this, new String[]{id, "" + y});
	}
}
