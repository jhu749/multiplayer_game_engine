package command;

import gameClient.MultiplayerActor;
import gameClient.MultiplayerWorld;

public class SetZOrderCommand extends ClientCommandHandler {

	@Override
	public void doCommand(String cmd, String[] params, MultiplayerWorld world) {
		if (params.length < 2) throw new IllegalArgumentException("Not enough parameters. Correct params are <id, x>");
		String id = params[0];
		int zOrder = Integer.parseInt(params[1]);
		MultiplayerActor actor = world.actorWithId(id);
		if (actor != null) {
			actor.setZOrder(zOrder);
		}
	}

	@Override
	public String getCommandWord() {
		return "zorder";
	}

	public String getSetZOrderCmdStr(String id, int zOrder) {
		return getCommandString(this, new String[]{id, "" + zOrder});
	}
}
