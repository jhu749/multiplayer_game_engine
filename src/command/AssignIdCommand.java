package command;

import gameClient.MultiplayerWorld;

public class AssignIdCommand extends ClientCommandHandler {

	@Override
	public void doCommand(String cmd, String[] params, MultiplayerWorld world) {
		if (params.length < 1) throw new IllegalArgumentException("Not enough parameters. Correct params are <id>");
		String id = params[0];
		world.getClient().setId(id);
	}

	@Override
	public String getCommandWord() {
		return "ID";
	}

	public String getAssignIdCmdStr(String id) {
		return getCommandString(this, new String[]{id});
	}
}
