package command;

import java.util.HashMap;

import gameServer.ServerWorld;

public abstract class ServerCommandHandler extends CommandHandler {
	private static HashMap<String, ServerCommandHandler> commandMap = initCommandMap(ServerCommandHandler.class);
	
	public static void doCommand(String cmdStr, ServerWorld world) {
		if (cmdStr == null || cmdStr.length() == 0) throw new IllegalArgumentException("cmdStr cannot be null or an empty string.");
		String cmd = getCommandWord(cmdStr);
		ServerCommandHandler commandHandler = commandMap.get(cmd);
		if (commandHandler == null) throw new IllegalArgumentException(cmd + " is not a valid command.");
		String[] params = getCommandParameters(cmdStr);
		commandHandler.doCommand(cmd, params, world);
	}
	
	/**
	 * Perform the command with the given parameters in the given world
	 * @param cmd The command
	 * @param params An array of parameters
	 * @param world The world in which to perform the command
	 */
	public abstract void doCommand(String cmd, String[] params, ServerWorld world);
	
}
