package command;

import java.lang.reflect.InvocationTargetException;

import gameClient.MultiplayerActor;
import gameClient.MultiplayerWorld;

public class AddActorCommand extends ClientCommandHandler {

	@Override
	public void doCommand(String cmd, String[] params, MultiplayerWorld world) {
		if (params.length < 8) throw new IllegalArgumentException("Not enough parameters. Correct params are <id, x, y>");
		String id = params[0];
		String clsName = params[1];
		try {
			Class<?> cls = Class.forName(clsName);
			MultiplayerActor p = world.createActor(cls, id);
			double x = Double.parseDouble(params[2]);
			double y = Double.parseDouble(params[3]);
			double scaleX = Double.parseDouble(params[4]);
			double scaleY = Double.parseDouble(params[5]);
			double rotation = Double.parseDouble(params[6]);
			int zOrder = Integer.parseInt(params[7]);
			p.setZOrder(zOrder);
	    	p.setX(x);
	    	p.setY(y);
	    	p.setScaleX(scaleX);
	    	p.setScaleY(scaleY);
	    	p.setRotate(Math.toDegrees(rotation));
			world.add(p);
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getCommandWord() {
		return "AA";
	}
	
	public String getAddActorCmdStr(String id, Class<?> cls, double x, double y, double scaleX, double scaleY, double rotation, int zOrder) {
		return getCommandString(this, new String[]{id, cls.getName(), "" + x, "" + y, "" + scaleX, "" + scaleY, "" + rotation, "" + zOrder});
	}
}
