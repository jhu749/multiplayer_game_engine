package command;

import gameClient.MultiplayerActor;
import gameClient.MultiplayerWorld;

public class SetRotationCommand extends ClientCommandHandler {

	@Override
	public void doCommand(String cmd, String[] params, MultiplayerWorld world) {
		if (params.length < 2) throw new IllegalArgumentException("Not enough parameters. Correct params are <id, y>");
		String id = params[0];
		double rotation = Double.parseDouble(params[1]);
		MultiplayerActor actor = world.actorWithId(id);
		if (actor != null) {
			actor.setRotate(Math.toDegrees(rotation));
		}
	}

	@Override
	public String getCommandWord() {
		return "R";
	}

	public String getSetRotationCmdStr(String id, double rotation) {
		return getCommandString(this, new String[]{id, "" + rotation});
	}
}
