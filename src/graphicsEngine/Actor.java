package graphicsEngine;

import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

import gameServer.CollisionMap;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.transform.Transform;

public abstract class Actor extends ImageView {
	
	private int zOrder;
	private double centerX;
	private double centerY;
	
	public Actor() {
		zOrder = 0;
		centerX = 0;
		centerY = 0;
		xProperty().addListener((observable, oldValue, newValue)-> {
			centerX += newValue.doubleValue() - oldValue.doubleValue();
		});
		
		yProperty().addListener((observable, oldValue, newValue)-> {
			centerY += newValue.doubleValue() - oldValue.doubleValue();
		});
		
		imageProperty().addListener((observable, oldValue, newValue)-> {
			centerX = getX() + getWidth() / 2;
			centerY = getY() + getHeight() / 2;
		});
	}
	
	public void setZOrder(int z) {
		zOrder = z;
		World w = getWorld();
		if (w != null) {
			w.zOrderChanged(this);
		}
	}
	
	public int getZOrder() {
		return zOrder;
	}
	
	/**
	 * Moves this actor by the given dx and dy.
	 * @param dx the amount to move horizontally (change in x)
	 * @param dy the amount to move vertically (change in y)
	 */
	public void move(double dx, double dy) {
		setX(getX() + dx);
		setY(getY() + dy);
	}
	
	/**
	 * returns the world this actor is in, or null if it is not in a world.
	 * @return the world this actor is in, or null if it is not in a world
	 */
	public World getWorld() {
		return (World)getParent();
	}
	
	/**
	 * Returns The width of the current image of this actor.
	 * @return the width of the current image of this actor, taking into account any transformations.
	 */
	public double getWidth() {
		return getBoundsInParent().getWidth();
	}
	
	/**
	 * Returns The height of the current image of this actor.
	 * @return the height of the current image of this actor, taking into account any transformations.
	 */
	public double getHeight() {
		return getBoundsInParent().getHeight();
	}
	
	public CollisionMap getTransformedCroppedCollisionMap() {
		Image img = getImage();
		if (img != null) {
			PixelReader reader = img.getPixelReader();
			if (reader != null) {
				CollisionMap map = new CollisionMap(SwingFXUtils.fromFXImage(img, null));
				AffineTransform trans = new AffineTransform();
				for (Transform t : getTransforms()) {
					AffineTransform tr = new AffineTransform(t.getMxx(), t.getMxy(), t.getTx(), t.getMyx(),t.getMyy(), t.getTy());
					trans.concatenate(tr);
				}
				trans.rotate(Math.toRadians(getRotate()), img.getWidth() / 2, img.getHeight() / 2);
				trans.scale(getScaleX(), getScaleY());
				return map.getTransformedMap(trans).getTrimmedMap();
			}
		}
		return null;
	}
	
	/**
	 * Returns a list of all the actors intersecting this actor.
	 * @return a list of all the actors intersecting this actor
	 */
	public List<Actor> getIntersectingObjects() {
		return getIntersectingObjects(Actor.class);
	}
	
	/**
	 * Returns a list of the actors of a given type intersecting this actor.
	 * @param <A> the class of intersecting actors that will be in the returned list
	 * @param cls The type of intersecting actors that should be in the list
	 * @return a list of all actors of the given type intersecting this actor
	 */
	public <A extends Actor> List<A> getIntersectingObjects(Class<A> cls) {
		ArrayList<A> list = new ArrayList<>();
		for (A a : getWorld().getObjects(cls)) {
			if (a != this && a.intersects(getBoundsInParent())) list.add(a);
		}
		return list;
	}
	
	/**
	 * Returns one actor of the given class that is intersecting this actor.
	 * @param <A> the class of intersecting actor that will be in the returned
	 * @param cls the type of actor to return
	 * @return an intersecting actor of the given class, or null if no such actor
	 */
	public <A extends Actor> A getOneIntersectingObject(Class<A> cls) {
		for (A a : getWorld().getObjects(cls)) {
			if (a != this && a.intersects(getBoundsInParent())) return a;
		}
		return null;
	}
	
	/**
	 * Sets the image for this actor to the image in the given fileName assuming the file name is in a source folder
	 * and accessible with getClass().getClassLoader().getResource(fileName)
	 * @param fileName the file name of the image
	 */
	public void setImage(String fileName) {
		Image playerImage = new Image(getClass().getClassLoader().getResource(fileName).toString());
		setImage(playerImage);
	}
	
	/**
	 * This method is called every frame once start has been called on the world.
	 * @param now the current time in nanoseconds.
	 */
	public abstract void act(long now);
	
	/**
	 * This method is called when an actor is added to the world using the
	 * add method of a world.
	 */
	public void addedToWorld() {
		// meant to be overridden.
	}
	
	/**
	 * This method is called when an actor is removed from the world
	 */
	public void removedFromWorld(World world) {
		
	}

	public double getCenterX() {
		return centerX;
	}

	public double getCenterY() {
		return centerY;
	}

}
