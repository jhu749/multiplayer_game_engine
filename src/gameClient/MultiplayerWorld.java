package gameClient;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ConcurrentHashMap;

import graphicsEngine.Actor;
import graphicsEngine.World;

public abstract class MultiplayerWorld extends World {
	private ConcurrentHashMap<String, MultiplayerActor> multiplayerActors;
	private GameClient client;
	
	public MultiplayerWorld(GameClient client) {
		multiplayerActors = new ConcurrentHashMap<>();
		this.client = client;
	}

	public MultiplayerActor actorWithId(String id) {
		return multiplayerActors.get(id);
	}
	
	@Override
	public void add(Actor a) {
		if (a instanceof MultiplayerActor) add((MultiplayerActor)a);
		else super.add(a);
	}
	
	public void add(MultiplayerActor a) {
		super.add(a);
		multiplayerActors.put(a.getActorId(), a);
	}
	
	@Override
	public void remove(Actor a) {
		if (a instanceof MultiplayerActor) remove((MultiplayerActor)a);
		else super.remove(a);
	}
	
	public void remove(MultiplayerActor a) {
		super.remove(a);
		multiplayerActors.remove(a.getActorId());
	}

	public GameClient getClient() {
		return client;
	}

	public void setClient(GameClient client) {
		this.client = client;
	}
	
	public MultiplayerActor createActor(Class<?> cls, String id) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		if (!MultiplayerActor.class.isAssignableFrom(cls)) throw new IllegalArgumentException(cls.getName() + " is not a subclass of MultiplayerActor");
		return (MultiplayerActor) cls.getConstructor(String.class).newInstance(id);
	}
	
	public abstract MultiplayerActor createPlayer(String id);
}
