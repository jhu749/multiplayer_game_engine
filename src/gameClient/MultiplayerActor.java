package gameClient;
import graphicsEngine.Actor;

public abstract class MultiplayerActor extends Actor {
	private String actorId;
	
	public MultiplayerActor(String actorId) {
		this.actorId = actorId;
	}

	public String getActorId() {
		return actorId;
	}
	
}
