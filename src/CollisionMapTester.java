

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import gameServer.CollisionMap;

public class CollisionMapTester {

	public static void main(String[] args) {
		InputStream stream = CollisionMap.class.getClassLoader().getResourceAsStream("playerShip2_red.png");
		try {
			BufferedImage img = ImageIO.read(stream);
			CollisionMap map = new CollisionMap(img);
			System.out.println(map);
			System.out.println();
			System.out.println();
			System.out.println();
			CollisionMap rot = map.getRotatedMap(Math.toRadians(45));//new CollisionMap(map, Math.toRadians(45));
			FileWriter fw = new FileWriter(new File("output.txt"));
			fw.write(rot.toString());
			fw.close();
//			CollisionMap scaled = rot.getScaledMap(0.3, 0.3);
//			System.out.println(scaled);
//			FileWriter fw = new FileWriter(new File("output.txt"));
//			fw.write(rot.toString());
//			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
